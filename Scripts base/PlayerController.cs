using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed = 15f; // La vitesse de déplacement du joueur
    public float backgroundWidth = 5f; // La largeur de la zone de jeu
    private Rigidbody2D _playerRb; // Variable qui va contenir le Rigidbody 2D du joueur

    // Start() est appelé au démarrage du jeu
    void Start()
    {
        // TODO : Récupère le Rigidbody 2D du joueur
    }

    // FixedUpdate() est appellé régulièrement mais ne dépend pas des FPS (image par second)
    // On utilise FixedUpdate() pour modifier la physique de notre jeu (la position du joueur dans notre cas)
    void FixedUpdate() 
    {
        // TODO : Récupère la saisie clavier du joueur (Le déplacement horizontal)

        // TODO : Calcul la nouvelle position du joueur       

        // TODO : Déplace le joueur à la nouvelle position calculée
        
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        // TODO : Recharge le niveau pour recommencer la partie
    }
}
