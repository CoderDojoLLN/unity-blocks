using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    public Transform[] spawnPoints; // Un tableau contenant les position où les blocs apparaisent
    public GameObject blockPrefab; // Le prefab 'Block'
    private float timerToSpawn = 2f; // Variable timer dans laquelle est calculé le moment d'apparition des blocs
                                     // La première apparition est 2 seconds après le démarage du jeu
    private float timeBetweenWaves = 1f; // Le temps en seconde entre chaque vague de blocs

    // Update() est appelé chaque image par seconde
    void Update()
    {
        // TODO : Vérifie le timer pour savoir s'il faut ajouter des blocs à l'écran

        // TODO : Si oui, appelle la méthode de création de blocs

        // TODO : Calcul le prochain timer
        
    }

    void SpawnBlocks()
    {
        // TODO : Détermine aléatoirement la position vide de la ligne de blocs

        // TODO : Pour chaque position de blocs (spawnPoints) ...

        // TODO : ... vérifie que ce n'est pas la position vide.

        // TODO : Si ce n'est pas la position vide, affiche un bloc à l'écran
        
    }
}
